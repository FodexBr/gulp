npm install browser-sync --save-dev


const browserSync = require("browserSync");

gulp.task("server", () => {
	browserSync.init({
		server: {
			baseDir: "dist"
			//proxy: "localhost:3000" => habilitando um proxy, para poder trabalhar com outros server's(Tomcat/Wildfly por exemplo)
		}
	});

	//Coloca um watcher de 'change', quando o diretorio dist mudar algum arquivo
	gulp.watch("dist/**/*").on('change', browserSync.reload);
});
