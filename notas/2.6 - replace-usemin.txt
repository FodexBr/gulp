** INstalando o Gulp-Usemin para fazer o replace nas paginas .html
	
	@Instalando o plugin: "gulp-usemin"
	## npm install gulp-usemin --save-dev
	## npm install gulp-cssmin --save-dev

	@Utilizando o plugin:
	gulp.task("usemin", () => {
		gulp.src("/app/**/*.html")
		.pipe(usemin({
			js: [uglify],
			css: [cssmin]
		}))
		.pipe(gulp.dest("/dist"))
	})

		<!-- build:js file.min.js -->
		<script>...</script>
		<script>...</script>
		<script>...</script>
		<!-- endbuild -->


		<!-- build:css file.min.css -->
		<link>...</link>
		<link>...</link>
		<link>...</link>
		<!-- endbuild -->