const express = require("express"),
	  app = express(),
	  compression = require("compression"),
	  helmet = require("helmet");

app.use(compression());
app.use(helmet());
app.use(express.static('../client/dist/'));

app.listen(3000, () => console.log("Server is running on port 3000"));
