/*
	Como Funciona: fluxo de leitura(src), e fluxo de destino(dest), o pipe é o
	que liga os dois fluxos de leitura, e dentro dele podemos colocar os plugins
	do gulpJS
	
	 ## gulp.task("nome-da-task", function() {
		gulp.src('from/**\/*')
		   .pipe(plugin())
		   .pipe(gulp.dest("to"));
	});

	## ** => todos os diretorios, baseado no path
	## * =>  todos os aquivos dos diretorios, baseado no path

*/




/* 
importando o module do gulp e seus plugins
*/
const gulp = require("gulp"),
	  imagemin = require("gulp-imagemin"),
	  clean = require("gulp-clean"),
	  concat = require("gulp-concat"),
	  htmlReplace = require('gulp-html-replace'),
	  uglify = require("gulp-uglify"),
	  usemin = require("gulp-usemin"),
	  cssmin = require("gulp-cssmin"),
	  browserSync = require('browser-sync'),
	  jsHint = require("gulp-jshint"),
	  jshintStylish = require('jshint-stylish'),
	  csslint = require('gulp-csslint'),
	  autoprefixer = require('gulp-autoprefixer'),
	  less = require('gulp-less');


/*
Deletando a pasta dist(Produção/Distribuição)
com o "return" o gulp espera essa tarefa acabar para executar as demais dependencias
*/
gulp.task("clean", () => {
	return gulp.src("dist").pipe(clean());
});

/*
Copiando pasta app(Desenvolvimento) para dist(Produção/Distribuição)
*/
gulp.task("copy",['clean'], () => {
	return gulp.src("app/**/*")
			   .pipe(gulp.dest('dist'));
});

/*
Otimizações de Imagens
*/
gulp.task('optimize-img', () => {
	return gulp.src('app/img/**/*')
			   .pipe(imagemin())
			   .pipe(gulp.dest("dist/img"));
});

/* ##Concatenando e Minificando arquivos javascript# #

gulp.task("optmize-js", () => {
	return gulp.src(['app/js/jquery.js', 'app/js/home.js', 'app/js/produto.js'])
		.pipe(concat('all.js'))
		.pipe(uglify())
		.pipe(gulp.dest("dist/js"));
});


## Replace com gulp-html-replace : meta-info build:js 
gulp.task("replace", () => {
	return gulp.src("dist/**\/*.html")
			   .pipe(htmlReplace({
			   		js: 'js/all.js',
			   }))
			   .pipe(gulp.dest('dist'));
});

*/

/*
Usemin que substitui o "gulp-html-replace"
e agrupa o uglify(minificacao), concat(concatencacao)
e o gulp-html-replace(replace por meta-informacao)

	// 1 - Minificar(Uglify ==> js, cssmin ==> css)W
*/
gulp.task("usemin", () => {
	gulp.src("app/**/*.html")
	.pipe(usemin({
		'js': [uglify],
		'css': [autoprefixer, cssmin]
	}))
	.pipe(gulp.dest("dist"));

});


gulp.task("server", () => {
	browserSync.init({
		server: {
			baseDir: "app"
		}
	});

	gulp.watch("app/**/*").on('change', browserSync.reload);


	gulp.watch("appjs/**/*.js").on('change', event => {
		console.log("Arquivo Modificado: " + event.path);
		gulp.src(event.path)
		.pipe(jsHint())
		.pipe(jsHint.reporter(jshintStylish)); // reporta os erros diretamento no Terminal
	});


	gulp.watch("app/css/**/*.css").on('change', event => {
		gulp.src(event.path)
			.pipe(csslint())
			.pipe(csslint.reporter());
	});



    gulp.watch('src/less/**/*.less').on('change', function(event) {
       var stream = gulp.src(event.path)
            .pipe(less().on('error', function(erro) {
              console.log('LESS, erro compilação: ' + erro.filename);
              console.log(erro.message);
            }))
            .pipe(gulp.dest('src/css'));
    });
});





gulp.task("default", ['copy'], function() {
	gulp.start('optimize-img', 'usemin');
});